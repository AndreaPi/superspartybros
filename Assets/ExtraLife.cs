﻿using System.Collections;
using UnityEngine;

public class ExtraLife : MonoBehaviour
{
    public int lifesAmount = 1;
    public bool taken = false;
    public GameObject explosion;

    // if the player touches the victory object, it has not already been taken, and the player can move (not dead or victory)
    // then the player has reached the victory point of the level
    void OnTriggerEnter2D(Collider2D other)
    {
        if ((other.tag == "Player") && (!taken) && (other.gameObject.GetComponent<CharacterController2D>().playerCanMove))
        {
            // mark as taken so doesn't get taken multiple times
            taken = true;

            // if explosion prefab is provide, then instantiate it
            if (explosion)
            {
                Instantiate(explosion, transform.position, transform.rotation);
            }

            // do the player gain lifes
            other.gameObject.GetComponent<CharacterController2D>().GainExtraLifes(lifesAmount);


            // destroy the extra life gameobject
            DestroyObject(this.gameObject);
        }
    }
}
